/*
 * Name: $RCSfile: CustomEditText.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Apr 5, 2013 4:10:57 PM $
 *
 * Copyright (C) 2012 COMPANY NAME, Inc. All rights reserved.
 */
package com.netviet.xfilm.ui;

/**
 * @author vidp
 *
 */


import com.netviet.xfilm.R;
import com.netviet.xfilm.utility.Utility;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

public class CustomEditText extends EditText {
    private Drawable dRight;
    private Rect rBounds;
    private int iconWidth;

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setIconWidth(context);
        addTextChangedListener(textWather);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setIconWidth(context);

        addTextChangedListener(textWather);
    }

    public CustomEditText(Context context) {
        super(context);
        setIconWidth(context);
        addTextChangedListener(textWather);
    }


    private void setIconWidth(Context context) {
        iconWidth = (int) Utility.convertDpToPixel(20, context);
    }

    private TextWatcher textWather = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            if (getText().length() >= 1) {
                Resources res = getResources();
//                Drawable img = res.getDrawable(R.drawable.btn_delete_pic);
//                img.setBounds(0, 0, iconWidth, iconWidth);
//                setCompoundDrawables(null, null, img, null);
            } else {
                setCompoundDrawables(null, null, null, null);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }
    };


    @Override
    public void setCompoundDrawables(Drawable left, Drawable top,
                                     Drawable right, Drawable bottom) {
        if (right != null) {
//            Resources res = getResources();
//            dRight = res.getDrawable(R.drawable.btn_delete_pic);
//            Rect _rctBound = right.getBounds();
//            dRight.setBounds(_rctBound.left - 40, _rctBound.top - 40, _rctBound.right + 40, _rctBound.bottom + 40);
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP && dRight != null) {
            rBounds = dRight.getBounds();
            final int x = (int) event.getX();
            final int y = (int) event.getY();
            if (x >= (this.getRight() - rBounds.width())
                    && x <= (this.getRight() - this.getPaddingRight())
                    && y >= this.getPaddingTop()
                    && y <= (this.getHeight() - this.getPaddingBottom())) {
                this.setText("");
                event.setAction(MotionEvent.ACTION_CANCEL);
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void finalize() throws Throwable {
        dRight = null;
        rBounds = null;
        super.finalize();
    }
}