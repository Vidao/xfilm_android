package com.netviet.xfilm.ui;

import java.util.Date;

import javax.net.ssl.X509KeyManager;

import com.netviet.xfilm.R;
import com.netviet.xfilm.utility.Utility;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class HitFilmView extends View {

	private int width;
	private int heigh;
	private Context context;
	private Rect rct_footer;
	private Rect rct_left;
	private Rect rct_right;
	private Rect rct_main;
	private int x_start = 0;
	private int image_bottom;
	private int image_width;
	private Drawable drb_left;
	private Drawable drb_right;
	private Drawable drb_main;
	private Rect[] rct_resources;
	private Drawable[] drb_resourdces;
	private int mainIndex = 1;
	private int numberOfHitFirm = 3;

	public int getNumberOfHitFirm() {
		return numberOfHitFirm;
	}

	public void setNumberOfHitFirm(int numberOfHitFirm) {
		this.numberOfHitFirm = numberOfHitFirm;
	}

	public HitFilmView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.context = context;
		drb_left = getResources().getDrawable(R.drawable.st);
		drb_main = getResources().getDrawable(R.drawable.nd);
		drb_right = getResources().getDrawable(R.drawable.rd);
	}

	@Override
	protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
		super.onSizeChanged(xNew, yNew, xOld, yOld);
		width = xNew;
		heigh = yNew;
		image_width = width / 4;
		image_bottom = heigh - 40;
		rct_footer = new Rect(0, image_bottom, width, heigh);
		rct_resources = new Rect[numberOfHitFirm];
		drb_resourdces = new Drawable[numberOfHitFirm];
		for (int i = 0; i < numberOfHitFirm; i++) {
			rct_resources[i] = new Rect();

		}
		drb_resourdces[0] = getResources().getDrawable(R.drawable.st);
		drb_resourdces[1] = getResources().getDrawable(R.drawable.nd);
		drb_resourdces[2] = getResources().getDrawable(R.drawable.rd);
		recoundRect();
		// drb_left.setBounds(rct_left);
		// drb_main.setBounds(rct_main);
		// drb_right.setBounds(rct_right);

	}

	private void recoundRect() {

		for (int i = 0; i < numberOfHitFirm; i++) {
			rct_resources[i].set(x_start + (i - mainIndex) * image_width * 2
					+ image_width, 0, x_start + (i - mainIndex + 1)
					* image_width * 2 + image_width, image_bottom);

			drb_resourdces[i].setBounds(rct_resources[i]);
		}

	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		canvas.drawRect(rct_footer, paint);
		// paint.setColor(Color.LTGRAY);
		// canvas.drawRect(rct_left, paint);
		// paint.setColor(Color.DKGRAY);
		// canvas.drawRect(rct_main, paint);
		// paint.setColor(Color.LTGRAY);
		// canvas.drawRect(rct_right, paint);
		for (Drawable _drb : drb_resourdces) {
			_drb.draw(canvas);
		}

	}

	private int x_down = 0;
	private int y_down = 0;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			// _downTime = new Date();
			x_down = (int) event.getX();

		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			double _x = event.getX();
			double _y = event.getY();
			invalidate();
			System.out.println("left:::::::::::: " + rct_resources[2].left
					+ " width::::::::: " + width + " right: :: : :"
					+ rct_resources[2].right);
		} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			double _xMove = event.getX();
			double _deltaX = x_down - _xMove;
			x_down = (int) _xMove;
			x_start -= _deltaX;
			if (rct_resources[numberOfHitFirm - 1].right <= width + 20) {
				scrollRight();
			}
			recoundRect();
			invalidate();
		}
		return true;
	}

	private void scrollRight() {
		// Rect _rct_buffer = new Rect(rct_resources[0]);
		Drawable _drb_buffer = drb_resourdces[0];

		for (int i = 0; i < numberOfHitFirm - 1; i++) {
			rct_resources[i] = new Rect(rct_resources[i + 1]);
			drb_resourdces[i] = drb_resourdces[i + 1];

		}
		drb_resourdces[numberOfHitFirm - 1] = _drb_buffer;
		rct_resources[numberOfHitFirm - 1] = new Rect(width * 4, 0, width * 6,
				image_bottom);
		x_start += 2 * image_width;
	}

}
