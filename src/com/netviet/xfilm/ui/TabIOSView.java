package com.netviet.xfilm.ui;

import com.google.android.gms.internal.fi;
import com.netviet.xfilm.ApplicationClass;
import com.netviet.xfilm.utility.Utility;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class TabIOSView extends View {

	private float width = ApplicationClass.SCREEN_WIDTH;
	private float heigh;
	private Paint paint = new Paint();
	private Paint paint_round_rect = new Paint();
	private int fontSize;
	private int selectedIndex = 0;
	private int xStart;
	private int xSecond;
	private int xLast;
	private int yStart;
	private int yEnd;
	private int xDirection;
	private RectF rct_backGround = new RectF();
	private int text1_width;
	private int text2_width;
	private int text3_width;
	private Path path = new Path();
	Point firstPoint;
	Point secondPoint;
	Point thirdPoint;
	private Rect rctFirstOnclick;
	private Rect rctSecondOnclick;
	private Rect rctThirdOnclick;
	private Handler parent_handler;

	public Handler getParent_handler() {
		return parent_handler;
	}

	public void setParent_handler(Handler parent_handler) {
		this.parent_handler = parent_handler;
	}

	public TabIOSView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setBackgroundColor(Color.rgb(35, 46, 52));
		paint.setColor(Color.WHITE);
		// paint.setFakeBoldText(true);
		fontSize = 30;
		paint.setTextSize(fontSize);
		heigh = Utility.convertDpToPixel(60, context);
		yStart = (int) (heigh / 2 - 8 - fontSize / 2);

		xStart = (int) (width / 5 - 25);
		xSecond = (int) (width * 2 / 5 - 25) + 30;
		xLast = (int) (width * 3 / 5 - 25) + 60;
		xDirection = xStart;
		paint_round_rect.setColor(Color.rgb(24, 32, 35));
		paint_round_rect.setStyle(Paint.Style.FILL);
		text1_width = (int) paint.measureText("Details");
		text1_width = text1_width + 50;
		text2_width = (int) paint.measureText("Ratings");
		text2_width = text2_width + 50;
		text3_width = (int) paint.measureText("Related");
		text3_width = text3_width + 50;
		countUIRedraw();
		rctFirstOnclick = new Rect(xStart + 15, yStart - 10, xStart
				+ text1_width - 40, yStart + 26 + fontSize);
		rctSecondOnclick = new Rect(xSecond + 15, yStart - 10, xSecond
				+ text2_width - 40, yStart + 26 + fontSize);
		rctThirdOnclick = new Rect(xLast + 15, yStart - 10, xLast + text3_width
				- 40, yStart + 26 + fontSize);
	}

	private void countUIRedraw() {

		int _width = text1_width;
		switch (selectedIndex) {
		case 0:
			_width = text1_width;
			break;
		case 1:
			_width = text2_width;
			break;
		case 2:
			_width = text3_width;
			break;

		default:
			break;
		}

		rct_backGround.set(xDirection, yStart, xDirection + _width, yStart
				+ fontSize + 25);

		firstPoint = new Point((xDirection + _width / 2) - 20, (int) heigh);
		secondPoint = new Point((xDirection + _width / 2), (int) heigh - 20);
		thirdPoint = new Point((xDirection + _width / 2) + 20, (int) heigh);
		path = new Path();
		path.setFillType(FillType.EVEN_ODD);
		path.moveTo(firstPoint.x, firstPoint.y);
		path.lineTo(secondPoint.x, secondPoint.y);
		path.lineTo(thirdPoint.x, thirdPoint.y);
		path.lineTo(firstPoint.x, firstPoint.y);

	}

	public TabIOSView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setBackgroundColor(Color.rgb(35, 46, 52));
	}

	public TabIOSView(Context context) {
		super(context);
		setBackgroundColor(Color.rgb(35, 46, 52));
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		canvas.drawRoundRect(rct_backGround, 20f, 20f, paint_round_rect);
		canvas.drawPath(path, paint_round_rect);
		if (selectedIndex == 0) {
			paint.setColor(Color.WHITE);
		} else {
			paint.setColor(Color.rgb(175, 175, 175));
		}
		canvas.drawText("Details", width / 5, heigh / 2 + fontSize / 2, paint);
		if (selectedIndex == 1) {
			paint.setColor(Color.WHITE);
		} else {
			paint.setColor(Color.rgb(175, 175, 175));
		}
		canvas.drawText("Ratings", width * 2 / 5 + 30,
				heigh / 2 + fontSize / 2, paint);
		if (selectedIndex == 2) {
			paint.setColor(Color.WHITE);
		} else {
			paint.setColor(Color.rgb(175, 175, 175));
		}
		canvas.drawText("Related", width * 3 / 5 + 60,
				heigh / 2 + fontSize / 2, paint);
	}

	private float x_down;
	private float y_down;
	private int downIndex = -1;

	boolean isMoving = false;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (isMoving) {
			return false;
		}
		final int action = event.getAction();
		switch (action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			downIndex = -1;
			x_down = event.getX();
			y_down = event.getY();
			if (rctFirstOnclick.contains((int) x_down, (int) y_down)) {
				downIndex = 0;
			} else if (rctSecondOnclick.contains((int) x_down, (int) y_down)) {
				downIndex = 1;
			} else if (rctThirdOnclick.contains((int) x_down, (int) y_down)) {
				downIndex = 2;
			}
			break;
		case MotionEvent.ACTION_MOVE:
			int _xMove = (int) event.getX();
			int _yMove = (int) event.getY();
			if ((downIndex == 0 && !rctFirstOnclick.contains(_xMove, _yMove))
					|| (downIndex == 1 && !rctSecondOnclick.contains(_xMove,
							_yMove))
					|| (downIndex == 2 && !rctThirdOnclick.contains(_xMove,
							_yMove))) {
				downIndex = -1;
			}
			break;
		case MotionEvent.ACTION_UP:
			if (downIndex == -1) {
				return false;
			} else {
				System.out.println("you've touch in " + downIndex);
				movingProcess(downIndex);
			}
			break;
		}

		return true;
	}

	private void movingProcess(int touchedIndex) {
		if (selectedIndex == touchedIndex) {
			return;
		}
		int _focus = touchedIndex > selectedIndex ? 1 : -1;
		int _xDelta = _focus * 10;
		new MovingBackGround(_xDelta, touchedIndex).start();

	}

	final Handler handlerValidate = new Handler() {
		public void handleMessage(android.os.Message msg) {
			countUIRedraw();
			invalidate();
			int _message = msg.what;
			if (_message == 1 && parent_handler != null) {
				// send parrent handler here;
//				System.out.println("selected index "+selectedIndex + " touched index ");

				switch (touchedIndex) {
				case 0:
					parent_handler
							.sendEmptyMessage(ApplicationClass.HANDLE_DETAIL_TAB);
					break;
				case 1:
					parent_handler
							.sendEmptyMessage(ApplicationClass.HANDLE_RATTING_TAB);
					break;
				case 2:
					parent_handler
							.sendEmptyMessage(ApplicationClass.HANDLE_RELATED_TAB);
					break;

				default:
					break;
				}
			} else if (_message == -1 && parent_handler != null) {
				switch (touchedIndex) {
				case 0:
					parent_handler
							.sendEmptyMessage(-ApplicationClass.HANDLE_DETAIL_TAB);
					break;
				case 1:
					parent_handler
							.sendEmptyMessage(-ApplicationClass.HANDLE_RATTING_TAB);
					break;
				case 2:
					parent_handler
							.sendEmptyMessage(-ApplicationClass.HANDLE_RELATED_TAB);
					break;

				default:
					break;
				}

			}
		};
	};

	private int touchedIndex;
	class MovingBackGround extends Thread {
		private int xDelta;
		private int destinationXDirection;
		

		public MovingBackGround(int _xDelta, int _touchedIndex) {
			xDelta = _xDelta;
			touchedIndex = _touchedIndex;
			switch (_touchedIndex) {
			case 0:
				destinationXDirection = xStart;
				break;
			case 1:
				destinationXDirection = xSecond;
				break;
			case 2:
				destinationXDirection = xLast;
				break;

			default:
				break;
			}
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (selectedIndex > touchedIndex) {
				handlerValidate.sendEmptyMessage(1);
			} else {
				handlerValidate.sendEmptyMessage(-1);
			}
			while ((xDelta > 0 && rct_backGround.left <= destinationXDirection)
					|| (xDelta < 0 && rct_backGround.left >= destinationXDirection)) {
				isMoving = true;
				xDirection += xDelta;
				handlerValidate.sendEmptyMessage(0);
				try {
					sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if ((xDelta > 0 && rct_backGround.left >= (destinationXDirection - 10))
						|| (xDelta < 0 && (rct_backGround.left <= destinationXDirection + 10))) {
					xDirection = destinationXDirection;
					isMoving = false;
					handlerValidate.sendEmptyMessage(0);
					selectedIndex = touchedIndex;
					break;
				}

			}
		}
	}

}
