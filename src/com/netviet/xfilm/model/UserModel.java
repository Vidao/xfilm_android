package com.netviet.xfilm.model;

public class UserModel {
	private String userName;
	private String realName;
	private String avatarHref;
	private String email;
	private int userID;
	private int userGem;
	private String userToken;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getAvatarHref() {
		return avatarHref;
	}
	public void setAvatarHref(String avatarHref) {
		this.avatarHref = avatarHref;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getUserGem() {
		return userGem;
	}
	public void setUserGem(int userGem) {
		this.userGem = userGem;
	}
	public String getUserToken() {
		return userToken;
	}
	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}
	
}
