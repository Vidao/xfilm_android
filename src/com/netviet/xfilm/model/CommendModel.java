package com.netviet.xfilm.model;

public class CommendModel {
	private int userID;
	private int userName;
	private long rateNumber;
	private String content;
	private String title;
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getUserName() {
		return userName;
	}
	public void setUserName(int userName) {
		this.userName = userName;
	}
	public long getRateNumber() {
		return rateNumber;
	}
	public void setRateNumber(long rateNumber) {
		this.rateNumber = rateNumber;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
