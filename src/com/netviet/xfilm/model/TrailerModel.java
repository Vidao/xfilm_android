package com.netviet.xfilm.model;

public class TrailerModel {
	private int trailerID;
	private int watchedNumber;
	private String title;
	private long duration;
	//0 youtube 1 from server;
	private int type;
	private String link;
	public int getTryerID() {
		return trailerID;
	}
	public void setTryerID(int tryerID) {
		this.trailerID = tryerID;
	}
	public int getWatchedNumber() {
		return watchedNumber;
	}
	public void setWatchedNumber(int watchedNumber) {
		this.watchedNumber = watchedNumber;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	

}
