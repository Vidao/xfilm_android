package com.netviet.xfilm.model;

public class FilmModel {
	private int filmID;
	private String title;
	private String thumbnail;
	private String description;
	private int watchedNumber;
	private float duration;
	private float IMDBRating;
	private float userRating;
	private int numberUserRating;
	public int getNumberUserRating() {
		return numberUserRating;
	}
	public void setNumberUserRating(int numberUserRating) {
		this.numberUserRating = numberUserRating;
	}
	private String genre;
	private String actor;
	private String director;
	private String relativedFilm;
	private int tryerID;
	private int link;
	private int price;
	private int quality;
	//youtybe or not
	private int type;
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuality() {
		return quality;
	}
	public void setQuality(int quality) {
		this.quality = quality;
	}
	public int getFilmID() {
		return filmID;
	}
	public void setFilmID(int filmID) {
		this.filmID = filmID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getWatchedNumber() {
		return watchedNumber;
	}
	public void setWatchedNumber(int watchedNumber) {
		this.watchedNumber = watchedNumber;
	}
	public float getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public float getIMDBRating() {
		return IMDBRating;
	}
	public void setIMDBRating(long iMDBRating) {
		IMDBRating = iMDBRating;
	}
	public float getUserRating() {
		return userRating;
	}
	public void setUserRating(float userRating) {
		this.userRating = userRating;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getActor() {
		return actor;
	}
	public void setActor(String actor) {
		this.actor = actor;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getRelativedFilm() {
		return relativedFilm;
	}
	public void setRelativedFilm(String relativedFilm) {
		this.relativedFilm = relativedFilm;
	}
	public int getTryerID() {
		return tryerID;
	}
	public void setTryerID(int tryerID) {
		this.tryerID = tryerID;
	}
	public int getLink() {
		return link;
	}
	public void setLink(int link) {
		this.link = link;
	}
	
	
}
