package com.netviet.xfilm.iview;

public interface IListCustomerListener {
	public void onIDClicked();

	public void onAgeClicked();

	public void onNameClicked();
}
