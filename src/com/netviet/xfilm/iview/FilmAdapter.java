package com.netviet.xfilm.iview;

import java.util.ArrayList;

import com.netviet.xfilm.R;
import com.netviet.xfilm.model.FilmModel;
import com.netviet.xfilm.utility.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class FilmAdapter extends BaseAdapter {

	private ArrayList<FilmModel> filmModelList;
	private Context context;
	DisplayImageOptions options;
	ImageSize imageSize;
	ImageLoader imageLoader = ImageLoader.getInstance();

	public FilmAdapter(Context context, ArrayList<FilmModel> filmModelList) {
		this.filmModelList = filmModelList;
		this.context = context;
		Utility.setImageConfig(context, 90);
		if (imageLoader.isInited()) {
			imageLoader.destroy();
		}
		imageLoader.init(Utility.imageLoaderConfig);
		options = getImageOption();
	}

	private DisplayImageOptions getImageOption() {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub)
				.showImageOnFail(R.drawable.loading_false)
				.showImageForEmptyUri(R.drawable.loading_false)
				.cacheInMemory(false).cacheOnDisc(true).build();
		return options;
	}

	@Override
	public int getCount() {
		return (filmModelList == null) ? 0 : filmModelList.size();
	}

	@Override
	public Object getItem(int position) {
		return (filmModelList == null) ? null : filmModelList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	private ViewHolder viewHolder;

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		FilmModel _filmModel = filmModelList.get(position);
		if (convertView == null) {
			LayoutInflater _inf = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = _inf.inflate(R.layout.film_row_item, null);
			viewHolder = new ViewHolder();
			viewHolder.iv_film_poster = (ImageView) convertView
					.findViewById(R.id.iv_film_poster);
			viewHolder.tv_title = (TextView) convertView
					.findViewById(R.id.tv_film_title);

			viewHolder.tv_view_duration = (TextView) convertView
					.findViewById(R.id.tv_view_duration);
			viewHolder.rb_rating = (RatingBar) convertView
					.findViewById(R.id.rb_rating);

			viewHolder.tv_rating_action = (TextView) convertView
					.findViewById(R.id.tv_rating_number);
			viewHolder.tv_description = (TextView) convertView
					.findViewById(R.id.tv_description);

			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		String _filmTitle = _filmModel.getActor() + ": "
				+ _filmModel.getTitle();
		viewHolder.tv_title.setText(_filmTitle);
		String _filmView = "View: " + _filmModel.getWatchedNumber()
				+ " | Duaration: " + _filmModel.getDuration();
		viewHolder.tv_view_duration.setText(_filmView);
		viewHolder.rb_rating.setRating(_filmModel.getUserRating());
		int _numberOfUserRating = _filmModel.getNumberUserRating();
		String _rating = " Rattings | ";
		if (_numberOfUserRating <= 1) {
			_rating = " Ratting | ";
		}
		String _filmNumberRating = _numberOfUserRating + _rating
				+ _filmModel.getGenre();
		viewHolder.tv_rating_action.setText(_filmNumberRating);
		String _filmDescription = _filmModel.getDescription();
		viewHolder.tv_description.setText(_filmDescription);
		// process for ImageLoader;
		loadingImage(imageSize, _filmModel.getThumbnail(),
				viewHolder.iv_film_poster);
		return convertView;
	}

	private void loadingImage(ImageSize targetSize, String pictureHref,
			final ImageView imageView) {
		imageLoader.displayImage(pictureHref, imageView);
//		imageLoader.loadImage(pictureHref, targetSize, options,
//				new SimpleImageLoadingListener() {
//					@Override
//					public void onLoadingStarted(String imageUri, View view) {
//						imageView.setImageResource(R.drawable.ic_stub);
//						super.onLoadingStarted(imageUri, view);
//					}
//
//					@Override
//					public void onLoadingComplete(String imageUri, View view,
//							Bitmap loadedImage) {
//						// Do whatever you want with Bitmap
//						// loadedImage.recycle();
//						// imageView.setImageDrawable(null);
//						imageView.setImageBitmap(loadedImage);
//						imageView.invalidate();
//					}
//				});
	}

	static class ViewHolder {
		ImageView iv_film_poster;
		TextView tv_title;
		TextView tv_view_duration;
		RatingBar rb_rating;
		TextView tv_rating_action;
		TextView tv_description;
	}

}
