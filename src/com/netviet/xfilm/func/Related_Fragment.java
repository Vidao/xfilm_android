package com.netviet.xfilm.func;

import java.util.ArrayList;

import com.netviet.xfilm.R;
import com.netviet.xfilm.iview.FilmAdapter;
import com.netviet.xfilm.iview.RelatedGalleryAdapter;
import com.netviet.xfilm.model.FilmModel;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageView;

public class Related_Fragment extends Fragment {

	private ArrayList<FilmModel> listFilmData = new ArrayList<FilmModel>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View _fragmentView = inflater.inflate(R.layout.fragment_related,
				container, false);
		
		initGallery(_fragmentView);
		return _fragmentView;
	}

	private void initGallery(View mainView) {
		GridView _gallery = (GridView) mainView.findViewById(R.id.gv_related);
		initData();
		RelatedGalleryAdapter _adapter = new RelatedGalleryAdapter(getActivity(), listFilmData);
		_gallery.setAdapter(_adapter);
		
	}

	private void initData() {
		FilmModel _model = new FilmModel();
		_model.setActor("Linh Thú");
		_model.setTitle("Phim 18+");
		_model.setWatchedNumber(8975);
		_model.setQuality(1);
		_model.setDuration(105);
		_model.setUserRating((float) 3.5);
		_model.setNumberUserRating(6);
		_model.setDescription("It’s great to be Spider-Man (Andrew Garfield). For Peter Parker, there’s no feeling quite like swinging between skyscrapers, embracing being the hero, and spending time with Gwen...");
		_model.setThumbnail("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSBYVn15rWW5A8ydapbaMU-5YgT868sRBo0fhmVqJIAJu90nKgi");
		for (int i = 0; i < 10; i++) {
			listFilmData.add(_model);
		}
	}

}
