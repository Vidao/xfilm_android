package com.netviet.xfilm.func;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.netviet.xfilm.ApplicationClass;
import com.netviet.xfilm.R;
import com.netviet.xfilm.ui.TabIOSView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class FilmDetailActivity extends FragmentActivity implements
		OnClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.film_detail);
		initUI();
	}

	DisplayImageOptions doption = null;
	private ImageLoadingListener animateFirstListener = null;

	private void initUI() {
		ImageView _ivMenuLeft = (ImageView) findViewById(R.id.btnMenu);
		_ivMenuLeft.setImageResource(R.drawable.back);
		_ivMenuLeft.setOnClickListener(this);
		ImageView _ivMenuRight = (ImageView) findViewById(R.id.btnRight);
		_ivMenuRight.setImageResource(R.drawable.favourite);
		_ivMenuRight.setOnClickListener(this);
		ImageView _ivFilmPoster = (ImageView) findViewById(R.id.iv_film_poster);
		_ivFilmPoster.setOnClickListener(this);
		ImageLoader _loader = ImageLoader.getInstance();
		// _loader.displayImage(
		// "http://powet.tv/powetblog/wp-content/uploads/2006/07/spiderman3poster.jpg",
		// _ivFilmPoster);

		TabIOSView _tabView = (TabIOSView) findViewById(R.id.tb_ios_view);
		_tabView.setParent_handler(tabHandler);
		doption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_stub)
				.showStubImage(R.drawable.ic_stub).cacheInMemory(false)
				.cacheOnDisc(true).displayer(new RoundedBitmapDisplayer(20))
				.build();
		animateFirstListener = new AnimateFirstDisplayListener();
		_loader.displayImage(
				"http://www.impawards.com/2003/posters/x_men_two_ver5_xlg.jpg",
				_ivFilmPoster, doption, animateFirstListener);

	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 1500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	private final Handler tabHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			boolean isLeft = false;

			if (msg.what < 0) {
				isLeft = true;
			}

			int _messageIndex = Math.abs(msg.what);
			switch (_messageIndex) {
			case ApplicationClass.HANDLE_DETAIL_TAB:
				// switch to detail fragment;
				selectFrag(0, isLeft);
				break;

			case ApplicationClass.HANDLE_RATTING_TAB:
				// swich to Ratting fragment;
				selectFrag(1, isLeft);
				break;
			case ApplicationClass.HANDLE_RELATED_TAB:
				// switch to Relatived fragment;
				selectFrag(2, isLeft);
				break;
			default:
				break;
			}
		};
	};

	public void selectFrag(int switchIndex, boolean isLeft) {
		Fragment fr = null;
		switch (switchIndex) {
		case 0:
			fr = new Detail_Fragment();
			break;
		case 1:
			fr = new Ratting_Fragment();
			break;
		case 2:
			fr = new Related_Fragment();
			break;
		default:
			break;
		}

		// FragmentManager fm = getFragmentManager();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
		// fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit,
		// R.anim.pop_enter, R.anim.pop_exit);
		// fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
		if (isLeft) {
			fragmentTransaction.setCustomAnimations(R.anim.enter_l,
					R.anim.exit_l);
		} else {
			fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
		}
		fragmentTransaction.replace(R.id.fg_detail, fr);
		// fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int _id = v.getId();
		switch (_id) {
		case R.id.iv_film_poster:
			gotoActivity(this, VideoPlayerActivity.class);
			break;
		default:
			break;
		}
	}

	public void gotoActivity(Context context, Class<?> cla) {
		Intent intent = new Intent(context, cla);
		startActivity(intent);
	}

}
