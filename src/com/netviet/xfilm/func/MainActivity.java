package com.netviet.xfilm.func;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.netviet.xfilm.ApplicationClass;
import com.netviet.xfilm.R;
import com.netviet.xfilm.iview.AppUtils;
import com.netviet.xfilm.iview.CarouselDataItem;
import com.netviet.xfilm.iview.CarouselView;
import com.netviet.xfilm.iview.CarouselViewAdapter;
import com.netviet.xfilm.iview.FilmAdapter;
import com.netviet.xfilm.iview.ISlideLeftMenuListenner;
import com.netviet.xfilm.iview.Singleton;
import com.netviet.xfilm.model.FilmModel;
import com.netviet.xfilm.utility.DialogManager;
import com.netviet.xfilm.utility.ParameterFactory;
import com.netviet.xfilm.utility.PreferencesManager;
import com.netviet.xfilm.utility.ToastManager;
import com.netviet.xfilm.utility.net.MyAsyncHttpPost;
import com.netviet.xfilm.utility.net.MyAsyncHttpResponseProcess;
import com.netviet.xfilm.view.FilmHintPointer;
import com.netviet.xfilm.view.LoginDialog;
import com.netviet.xfilm.view.SignUpDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class MainActivity extends BaseSlideActivity implements
		ISlideLeftMenuListenner, OnClickListener, OnItemSelectedListener {
	// for regist GCM at first time;
	public static final String EXTRA_MESSAGE = "message";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	static final String TAG = "GCMLOG";
	String SENDER_ID = ApplicationClass.PROJECT_NUMBER;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	String regid;
	private MyAsyncHttpPost asyncUpdateDeviceToken = null;
	private MyAsyncHttpPost asyncSignUp = null;
	private MyAsyncHttpPost asyncLogin = null;
	Singleton m_Inst = Singleton.getInstance();
	CarouselViewAdapter m_carouselAdapter = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = MainActivity.this;
		View view = inflater.inflate(R.layout.activity_main, null);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = getDisplaySize(display);
		ApplicationClass.SCREEN_WIDTH = size.x;
		ApplicationClass.SCREEN_HEIGH = size.y;
		int statusBarHeigh = getStatusBarHeight();
		ApplicationClass.SCREEN_HEIGH -= statusBarHeigh;

		addContentFrame(view);
		initUI();
	}

	private Point getDisplaySize(final Display display) {
		Point point = new Point();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) { // API
																			// LEVEL
																			// 13
			display.getSize(point);
		} else {
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		return point;
	}

	public int getStatusBarHeight() {
		int statusBarHeight = 0;

		if (!hasOnScreenSystemBar()) {
			int resourceId = getResources().getIdentifier("status_bar_height",
					"dimen", "android");
			if (resourceId > 0) {
				statusBarHeight = getResources().getDimensionPixelSize(
						resourceId);
			}
		}

		return statusBarHeight;
	}

	private boolean hasOnScreenSystemBar() {
		Display display = getWindowManager().getDefaultDisplay();
		int rawDisplayHeight = 0;
		try {
			Method getRawHeight = Display.class.getMethod("getRawHeight");
			rawDisplayHeight = (Integer) getRawHeight.invoke(display);
		} catch (Exception ex) {
		}

		int UIRequestedHeight = ApplicationClass.SCREEN_HEIGH;

		return rawDisplayHeight - UIRequestedHeight > 0;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (asyncUpdateDeviceToken != null) {
			asyncUpdateDeviceToken.setFlagStop(true);
		}
		if (asyncSignUp != null) {
			asyncSignUp.setFlagStop(true);
		}
		if (asyncLogin != null) {
			asyncLogin.setFlagStop(true);
		}

	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void registerInBackground() {
		new AsyncTask() {

			@Override
			protected void onPostExecute(Object result) {
				Log.d(TAG, result.toString());
			};

			@Override
			protected Object doInBackground(Object... arg0) {
				// TODO Auto-generated method stub
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(self);
					}
					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;

					// You should send the registration ID to your server over
					// HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your
					// app.
					// The request to your server should be authenticated if
					// your app
					// is using accounts.
					sendRegistrationIdToBackend(regid);

					// For this demo: we don't need to send it because the
					// device
					// will send upstream messages to a server that echo back
					// the
					// message using the 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId(self, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}
		}.execute(null, null, null);

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		// unregisterReceiver(mHandleMessageReceiver);
		isSTop = true;
		super.onDestroy();
	}

	private void sendRegistrationIdToBackend(final String deviceToken) {
		// send registID to server here;
		PreferencesManager _pref = PreferencesManager.getInstance(self);
		String userName = _pref
				.getStringValue(PreferencesManager.PREF_USER_NAME);
		String userToken = _pref
				.getStringValue(PreferencesManager.PREF_USER_TOKEN);

		List<NameValuePair> params = ParameterFactory.deviceTokenParam(
				userName, userToken, deviceToken);
		asyncUpdateDeviceToken = new MyAsyncHttpPost(self,
				new MyAsyncHttpResponseProcess(self) {
					@Override
					public void before() {
						// TODO Auto-generated method stub
						super.before();
					}

					@Override
					public void processIfResponseSuccess(String response) {
						PreferencesManager _pref = PreferencesManager
								.getInstance(self);
						_pref.putBooleanValue(
								PreferencesManager.PREF_IS_SEND_TOKEN, true);
					}

					@Override
					public void processIfResponseFail(String message) {
						// failed
						// DialogManager.alert(self, "Sign Up", message);
					}

					@Override
					public void processIfServerError() {
						// TODO Auto-generated method stub
						super.processIfServerError();
					}
				}, params);
		asyncUpdateDeviceToken.execute(ApplicationClass.URL_SET_DEVICE_TOKEN);

	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final PreferencesManager prefs = PreferencesManager.getInstance(self);
		int appVersion = ApplicationClass.getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);

		prefs.putStringValue(PreferencesManager.PREF_PROPERTY_REG_ID, regId);
		prefs.putIntValue(PreferencesManager.PREF_PROPERTY_APP_VERSION,
				appVersion);
	}

	private String getRegistrationId(Context context) {
		final PreferencesManager prefs = PreferencesManager.getInstance(self);
		String registrationId = prefs.getStringValue(
				PreferencesManager.PREF_PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs
				.getIntValue(PreferencesManager.PREF_PROPERTY_APP_VERSION,
						Integer.MIN_VALUE);
		int currentVersion = ApplicationClass.getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			prefs.putIntValue(PreferencesManager.PREF_PROPERTY_APP_VERSION,
					currentVersion);
			return "";
		}
		return registrationId;
	}

	private boolean isLogin = false;
	private String userName;
	private CarouselView coverFlow;

	FilmHintPointer hintPointer;
	private ArrayList<FilmModel> listFilmData = new ArrayList<FilmModel>();

	private void initUI() {
		Singleton.getInstance().InitGUIFrame(this);
		RelativeLayout panel = (RelativeLayout) findViewById(R.id.layout_hit);
		// panel.setPadding(0, padding, 0, padding);
		// create the carousel
		coverFlow = new CarouselView(this);
		hintPointer = (FilmHintPointer) findViewById(R.id.hintpoint);
		hintPointer.setXcenter();
		// create adapter and specify device independent items size (scaling)
		m_carouselAdapter = new CarouselViewAdapter(this, m_Inst.Scale(240),
				m_Inst.Scale(240));

		coverFlow.setAdapter(m_carouselAdapter);
		coverFlow.setSpacing(-1 * m_Inst.Scale(110));
		coverFlow.setSelection(Integer.MAX_VALUE / 2, true);
		coverFlow.setAnimationDuration(1000);
		coverFlow.setOnItemSelectedListener((OnItemSelectedListener) this);
		coverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				int _index = arg2 % 5;
				FilmModel _model = listFilmData.get(_index);
				int _filmID = _model.getFilmID();
				Bundle _bundle = new Bundle();
				_bundle.putInt("FILMID", _filmID);
				gotoActivity(self, FilmDetailActivity.class, _bundle);
			}
		});

		AppUtils.AddView(panel, coverFlow, LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT,
				new int[][] { new int[] { RelativeLayout.ALIGN_PARENT_TOP } },
				-1, -1);
		new Test().start();
		ListView _listFilm = (ListView) findViewById(R.id.lv_film);
		initData();
		FilmAdapter adapter = new FilmAdapter(self, listFilmData);
		_listFilm.setAdapter(adapter);
		_listFilm.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				FilmModel _model = listFilmData.get(position);
				int _filmID = _model.getFilmID();
				Bundle _bundle = new Bundle();
				_bundle.putInt("FILMID", _filmID);
				gotoActivity(self, FilmDetailActivity.class, _bundle);
			}
		});
	}

	private void initData() {
		FilmModel _model = new FilmModel();
		_model.setActor("Linh Thú");
		_model.setTitle("Phim 18+");
		_model.setWatchedNumber(8975);
		_model.setQuality(1);
		_model.setDuration(105);
		_model.setUserRating((float) 3.5);
		_model.setNumberUserRating(6);
		_model.setDescription("It’s great to be Spider-Man (Andrew Garfield). For Peter Parker, there’s no feeling quite like swinging between skyscrapers, embracing being the hero, and spending time with Gwen...");
		_model.setThumbnail("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSBYVn15rWW5A8ydapbaMU-5YgT868sRBo0fhmVqJIAJu90nKgi");
		for (int i = 0; i < 10; i++) {
			listFilmData.add(_model);
		}
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				DialogManager.alert(self, R.string.gcm_play_service_error);
			}
			return false;
		}
		return true;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		checkPlayServices();
		PreferencesManager _pref = PreferencesManager.getInstance(self);
		userName = _pref.getStringValue(PreferencesManager.PREF_USER_NAME);
		if (userName == null || userName.length() < 6) {
			// bt_login.setText("Login");
			isLogin = false;
		} else {
			// bt_login.setText("Logout");
			isLogin = true;
			menuListContent.setContext(self);
		}

	}

	@Override
	public void profileSelectedListener() {
		toggle();
	}

	@Override
	public void listCustomerSelectedListener() {
		toggle();
	}

	@Override
	public void homeSelectedListener() {
		toggle();
	}

	@Override
	public void preferenceSelectedListener() {
		showToastMessage("Clicked on preference!");
		toggle();
	}

	private void showToastMessage(String message) {
		Toast.makeText(self, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void quitSelectedListener() {
		finish();
	}

	@Override
	public void onClick(View v) {
		// int _viewID = v.getId();
	}

	private void logout() {
		PreferencesManager _pref = PreferencesManager.getInstance(self);
		_pref.putStringValue(PreferencesManager.PREF_USER_NAME, "");
		_pref.putStringValue(PreferencesManager.PREF_FACEBOOK_USER_TOKEN, "");
		// bt_login.setText("Login");
		isLogin = false;
	}

	private void processSign() {
		if (isLogin) {
			DialogManager.showConfirmDialog(self, "Do you want to logout",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							logout();
						}
					}, null);
		} else {
			showLoginDialog();
		}
	}

	final private Handler dialogHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			int messageCode = msg.what;
			switch (messageCode) {
			case LoginDialog.HANDLER_LOGIN:
				// process login
				Bundle _bunder = msg.getData();
				String _userName = _bunder.getString("USERNAME");
				String _passWord = _bunder.getString("PASSWORD");
				boolean _isSaving = _bunder.getBoolean("ISSAVING");
				login(_userName, _passWord, _isSaving);
				break;
			case LoginDialog.HANDLER_SIGNUP:
				// show signup dialog
				if (loginDialog != null) {
					loginDialog.dismiss();
				}
				showRegisterDialog();

				break;
			case SignUpDialog.HANDLER_SIGNUP_PROCESS:
				Bundle _signUpInfo = msg.getData();
				userName = _signUpInfo.getString("USERNAME");
				String _signup_passWord = _signUpInfo.getString("PASSWORD");
				String _signup_phone_number = _signUpInfo
						.getString("PHONENUMBER");
				// process sign up here;
				signup(userName, _signup_passWord, _signup_phone_number);

				break;
			case SignUpDialog.HANDLER_SIGNUP_RESPONSE:
				// donot use this case;
				break;

			default:
				break;
			}
		};
	};

	private void signup(final String userName, String passWord,
			String phoneNumber) {
		List<NameValuePair> params = ParameterFactory.signUpParam(userName,
				passWord, phoneNumber);
		asyncSignUp = new MyAsyncHttpPost(self, new MyAsyncHttpResponseProcess(
				self) {

			@Override
			public void before() {
				// TODO Auto-generated method stub
				super.before();
				showProgressDialog(false);
			}

			@Override
			public void processIfResponseSuccess(String response) {
				parseSignUp(response);

			}

			@Override
			public void processIfResponseFail(String message) {
				// failed
				DialogManager.alert(self, "Sign Up", message);
			}
		}, params);

		asyncSignUp.execute(ApplicationClass.URL_SIGN_UP);
	}

	private void parseSignUp(String response) {
		try {
			JSONObject _jsResponse = new JSONObject(response);
			String _errorCode = _jsResponse.getString("ERROR_CODE");
			String _message = _jsResponse.getString("MESSAGE");
			if (_errorCode.equals("0")) {
				signUpDialog.dismiss();
			} else {
				ToastManager.showLongToastMessage(self, _message);
				return;
			}

			int _userID = _jsResponse.getInt("ID");
			ToastManager.showLongToastMessage(self, _message);
			if ("0".equals(_errorCode)) {
				String _userToken = _jsResponse.getString("Token");
				// mark isLogin == true
				PreferencesManager pref = PreferencesManager.getInstance(self);
				pref.putBooleanValue(PreferencesManager.PREF_IS_SAVE_ACCOUNT,
						true);
				pref.putStringValue(PreferencesManager.PREF_USER_NAME, userName);
				pref.putStringValue(PreferencesManager.PREF_USER_TOKEN,
						_userToken);
				pref.putIntValue(PreferencesManager.PREF_USER_ID, _userID);
				// update device token;
				getDeviceToken();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private SignUpDialog signUpDialog;

	private void showRegisterDialog() {
		signUpDialog = new SignUpDialog(this, self);
		signUpDialog.setParentHandler(dialogHandler);
		signUpDialog.show();
	}

	private void login(final String userName, String passWord,
			final boolean isSaving) {
		List<NameValuePair> params = ParameterFactory.loginParam(userName,
				passWord);
		asyncLogin = new MyAsyncHttpPost(self, new MyAsyncHttpResponseProcess(
				self) {

			@Override
			public void before() {
				// TODO Auto-generated method stub
				super.before();
				showProgressDialog(false);
			}

			@Override
			public void processIfResponseSuccess(String response) {
				parseLogin(response, isSaving, userName);
			}

			@Override
			public void processIfResponseFail(String message) {
				// failed
				DialogManager.alert(self, "Login", message);
			}
		}, params);
		asyncLogin.execute(ApplicationClass.URL_LOGIN);
	}

	private void parseLogin(String response, boolean isSaving, String userName) {
		try {
			JSONObject _response = new JSONObject(response);
			String _errorCode = _response.getString("ERROR_CODE");
			String _message = _response.getString("MESSAGE");

			if ("0".equals(_errorCode)) {
				int _userID = Integer.parseInt(_response.getString("ID"));
				PreferencesManager _pref = PreferencesManager.getInstance(self);
				this.userName = userName;
				String _userToken = _response.getString("USERTOKEN");
				String _realName = _response.getString("REAL_NAME");
				String _email = _response.getString("EMAIL");
				String _avatar_href = _response.getString("AVATA_HREF");
				if (_realName != null && _realName.length() > 2) {
					ToastManager.showLongToastMessage(self, "Well come back "
							+ _realName);
				} else {
					ToastManager.showLongToastMessage(self, "Well come back "
							+ userName);
				}
				_pref.putStringValue(PreferencesManager.PREF_USER_NAME,
						userName);
				_pref.putStringValue(PreferencesManager.PREF_USER_TOKEN,
						_userToken);
				_pref.putBooleanValue(PreferencesManager.PREF_IS_SAVE_ACCOUNT,
						isSaving);
				_pref.putIntValue(PreferencesManager.PREF_USER_ID, _userID);
				_pref.putStringValue(PreferencesManager.PREF_USER_REAL_NAME,
						_realName);
				_pref.putStringValue(PreferencesManager.PREF_USER_EMAIL, _email);
				_pref.putStringValue(PreferencesManager.PREF_USER_AVARTAR_HREF,
						_avatar_href);
				// bt_login.setText("Logout");
				loginDialog.dismiss();
				getDeviceToken();

			} else {
				DialogManager.alert(self, _message);
			}

		} catch (JSONException jsEx) {
			jsEx.printStackTrace();
		}
	}

	private void getDeviceToken() {

		boolean _isPlayServiceReady = checkPlayServices();
		if (_isPlayServiceReady) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(self);
			if (regid.isEmpty()) {
				registerInBackground();
			} else {
				sendRegistrationIdToBackend(regid);
			}
		}
	}

	private LoginDialog loginDialog;

	private void showLoginDialog() {
		loginDialog = new LoginDialog(this, self);
		loginDialog.setParentHandler(dialogHandler);
		loginDialog.show();
	}

	private final Handler handler_switch_gallary = new Handler() {
		public void handleMessage(android.os.Message msg) {
			coverFlow.onKeyDown(KeyEvent.KEYCODE_DPAD_RIGHT, null);
			hintPointer.setActivedIndex(coverFlow.getSelectedItemPosition()
					% hintPointer.getNumberOfHint());
		};
	};

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		hintPointer.setActivedIndex(arg2 % 5);

	}

	private boolean isSTop = false;

	class Test extends Thread {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			while (!isSTop) {
				handler_switch_gallary.sendEmptyMessage(0);

				try {
					sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}
}
