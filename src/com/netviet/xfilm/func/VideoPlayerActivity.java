package com.netviet.xfilm.func;

import com.netviet.xfilm.ApplicationClass;
import com.netviet.xfilm.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoPlayerActivity extends Activity {

//	public static String path;
	private VideoView mVideoView;

	public Context context;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.videoplayer);

		getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// Get the size of the device, will be our maximum.
		// Display display = getWindowManager().getDefaultDisplay();
		// int screen_width = display.getWidth();
		// int screen_height = display.getHeight();
		if (!ApplicationClass.isRotate) {
			ApplicationClass.isRotate = true;
			Toast.makeText(this, "Xoay ngang màn hình để fullscreen", 5).show();
		}
		// showProgressDialog(false;)
		showSmallWait(true, this);
		mVideoView = (VideoView) findViewById(R.id.surface_view);
		context = this;
		Uri uri = Uri.parse("http://content.viki.com/test_ios/ios_240.m3u8");
		mVideoView.setVideoURI(uri);
		mVideoView.setMediaController(new MediaController(context));

//		if (path != null) {
			mVideoView.requestFocus();
			mVideoView.start();
			// Com.dimissWait();.
//		}

		new LoadingClass().start();

	}

	private boolean isShowWait = false;
	private ProgressDialog myDialog;

	public void showSmallWait(boolean isCancel, Context context) {
		if (!isShowWait) {
			myDialog = ProgressDialog.show(context, "Chờ chút nhé", "", true,
					isCancel);
			myDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					isShowWait = false;
					myDialog.dismiss();

				}
			});

			isShowWait = true;
		}

	}

	final Handler handler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			int state = msg.what;
			switch (state) {
			case 0:
				Toast.makeText(context, "Không tìm thấy đường dẫn này.", 5)
						.show();
				break;
			case 1:
				dimissWait();
				break;
			}
			dimissWait();
		}
	};

	class LoadingClass extends Thread {

		public void run() {
			try {
				sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			handler.sendEmptyMessage(11);
		}
	}

	public void dimissWait() {

		if (myDialog != null && isShowWait)
			myDialog.dismiss();
		isShowWait = false;
	}

}
