package com.netviet.xfilm.async;

import com.netviet.xfilm.utility.TimeCounter;

import android.os.AsyncTask;

/**
 * @author cuongvm6037
 */
public class TimeCounterTask extends AsyncTask<String, Long, Void> {

	public interface TimerListener {
		void before();

		void recording(float seconds);

		void after();
	}

	private TimerListener listener;
	private TimeCounter tc;
	private boolean running;

	public TimeCounterTask(TimerListener listener) {
		this.listener = listener;
	}

	public void stop() {
		running = false;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		listener.before();
		tc = new TimeCounter();
		running = true;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		listener.after();
	}

	@Override
	protected Void doInBackground(String... params) {
		while (running) {
			publishProgress(Long.valueOf(tc.countTime()));
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				break;
			}
		}
		return null;
	}

	@Override
	protected void onProgressUpdate(Long... values) {
		super.onProgressUpdate(values);
		long millis = values[0].longValue();
		listener.recording((float) millis / 1000);
	}
}
