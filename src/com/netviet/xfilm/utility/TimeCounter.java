package com.netviet.xfilm.utility;

import java.util.Date;

/**
 * @author cuongvm6037
 */
public class TimeCounter {
    private long startTime;

    public TimeCounter() {
        startTime = new Date().getTime();
    }

    public long countTime() {
        return new Date().getTime() - startTime;
    }
}