package com.netviet.xfilm.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {

	private static PreferencesManager instance;

	private Context context;

	private PreferencesManager() {
	}

	public static PreferencesManager getInstance(Context context) {
		if (instance == null) {
			instance = new PreferencesManager();
			instance.context = context;
		}
		return instance;
	}

	public final String XFILM_PREFERENCE_MANAGER = "XFILM_PREFERENCE_MANAGER";
	public static final String PREF_USER_TOKEN = "pref_user_token";
	public static final String PREF_IS_SAVE_ACCOUNT = "saving_account";
	public static final String PREF_IS_SEND_TOKEN = "sended_token";
	public static final String PREF_USER_NAME = "pref_user_name";
	public static final String PREF_USER_REAL_NAME = "pref_user_real_name";
	public static final String PREF_USER_EMAIL = "pref_user_email";
	public static final String PREF_USER_AVARTAR_HREF = "pref_user_avatar_href";
	public static final String PREF_USER_ID = "pref_user_id";
	public static final String PREF_USER_LOGINED = "pref_user_logined";
	public static final String PREF_RECORDER_SELECTED = "pref_recorder_selected";
	public static final String PREF_FACEBOOK_USER_NAME = "pref_face_book_username";
	public static final String PREF_FACEBOOK_USER_TOKEN = "pref_facebook_user_token";
	public static final String PREF_FACEBOOK_COUNT_TIME = "pref_facebook_count_time";
//	public static final String PREF_FACEBOOK_ACCESS_EXPIRES = "pref_facebook_access_expires";
	/*
	 * GCM
	 */
	public static final String PREF_PROPERTY_REG_ID = "registration_id";
	public static final String PREF_PROPERTY_APP_VERSION = "appVersion";

	/**
	 * Save a long integer to SharedPreferences
	 * 
	 * @param key
	 * @param n
	 */
	public void putLongValue(String key, long n) {
		// SmartLog.log(TAG, "Set long integer value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putLong(key, n);
		editor.commit();
	}

	/**
	 * Read a long integer to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public long getLongValue(String key) {
		// SmartLog.log(TAG, "Get long integer value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		return pref.getLong(key, 0);
	}

	/**
	 * Save an integer to SharedPreferences
	 * 
	 * @param key
	 * @param n
	 */
	public void putIntValue(String key, int n) {
		// SmartLog.log(TAG, "Set integer value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(key, n);
		editor.commit();
	}

	/**
	 * Read an integer to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public int getIntValue(String key) {
		// SmartLog.log(TAG, "Get integer value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		return pref.getInt(key, 0);
	}

	public int getIntValue(String key, int defaultValue) {
		// SmartLog.log(TAG, "Get string value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		return pref.getInt(key, defaultValue);
	}

	/**
	 * Save an string to SharedPreferences
	 * 
	 * @param key
	 * @param s
	 */
	public void putStringValue(String key, String s) {
		// SmartLog.log(TAG, "Set string value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, s);
		editor.commit();
	}

	/**
	 * Read an string to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public String getStringValue(String key) {
		// SmartLog.log(TAG, "Get string value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		return pref.getString(key, "");
	}

	/**
	 * Read an string to SharedPreferences
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public String getStringValue(String key, String defaultValue) {
		// SmartLog.log(TAG, "Get string value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		return pref.getString(key, defaultValue);
	}

	/**
	 * Save an boolean to SharedPreferences
	 * 
	 * @param key
	 * @param s
	 */
	public void putBooleanValue(String key, Boolean b) {
		// SmartLog.log(TAG, "Set boolean value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(key, b);
		editor.commit();
	}

	/**
	 * Read an boolean to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public boolean getBooleanValue(String key) {
		// SmartLog.log(TAG, "Get boolean value");
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		return pref.getBoolean(key, false);
	}

	/**
	 * Save an float to SharedPreferences
	 * 
	 * @param key
	 * @param s
	 */
	public void putFloatValue(String key, float f) {
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putFloat(key, f);
		editor.commit();
	}

	/**
	 * Read an float to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public float getFloatValue(String key) {
		SharedPreferences pref = context.getSharedPreferences(
				XFILM_PREFERENCE_MANAGER, 0);
		return pref.getFloat(key, 0.0f);
	}
}
