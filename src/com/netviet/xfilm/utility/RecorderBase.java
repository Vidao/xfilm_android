package com.netviet.xfilm.utility;


import android.media.AudioFormat;
import android.media.AudioRecord;

/**
 * @author cuongvm6037
 * 
 */
public abstract class RecorderBase {

	public static final int SAMPLE_RATE = 16000;

	public int getMinBufferSize() {
		return AudioRecord.getMinBufferSize(SAMPLE_RATE,
				AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT) * 2;
	}

}
