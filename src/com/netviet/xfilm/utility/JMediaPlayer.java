package com.netviet.xfilm.utility;

import java.io.IOException;

import android.media.MediaPlayer;

/**
 * @author vidp
 */
public class JMediaPlayer extends MediaPlayer {

    private static JMediaPlayer jMediaPlayer;
    private static long timeCurrent = System.currentTimeMillis();
    private static boolean isSession = false;

    /**
     *
     */
    private JMediaPlayer() {
    }

    /**
     * @return
     */
    public static synchronized JMediaPlayer getJMediaPlayer() {
        if (null == jMediaPlayer) {
            jMediaPlayer = new JMediaPlayer();
            isSession = true;
        }
        return jMediaPlayer;
    }

    /**
     *
     */
    @Override
    public void setDataSource(String path)
            throws IOException, IllegalArgumentException, SecurityException,
            IllegalStateException {
        if (isSession) {
            super.setDataSource(path);
        }
    }

    /**
     * @return
     */
    public boolean isNewSession() {
        return isSession;
    }

    /**
     *
     */
    public void releaseMemory() {
        if (System.currentTimeMillis() > timeCurrent + 500) {
            jMediaPlayer = null;
        }
        timeCurrent = System.currentTimeMillis();
        isSession = false;
    }
}
