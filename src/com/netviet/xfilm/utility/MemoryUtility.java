/*
 * Name: $RCSfile: MemoryUtility.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Nov 15, 2011 10:40:19 AM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.netviet.xfilm.utility;

/**
 * MemoryUtility
 *
 * @author Hai Le
 */
public class MemoryUtility {
    /**
     * Check if external storage exists such as SD card
     *
     * @return
     */
    public static boolean hasExternalStorage() {
        return android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }
}
