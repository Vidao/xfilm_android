/*
 * Name: $RCSfile: MyAsyncBabble.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Jun 6, 2012 9:57:43 AM $
 *
 * Copyright (C) 2012 COMPANY_NAME, Inc. All rights reserved.
 */
package com.netviet.xfilm.utility.net;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import com.netviet.xfilm.ApplicationClass;
import com.netviet.xfilm.utility.DirectoryManager;
import com.netviet.xfilm.utility.FileUtil;
import com.netviet.xfilm.utility.IOUtilException;
import com.netviet.xfilm.utility.MemoryUtility;
import com.netviet.xfilm.utility.NetworkUtility;

import android.content.Context;
import android.os.AsyncTask;

/**
 * MyAsyncBabble supports to download babble from server
 * 
 * @author Hai Lee
 * @edit khanhnv
 */
public class MyAsyncBabble extends AsyncTask<String, Integer, String> {
	// Network status
	public static final int NETWORK_STATUS_OK = 0;
	public static final int NETWORK_STATUS_OFF = 1;
	public static final int NETWORK_STATUS_ERROR = 2;

	public static final String TAG = "MyAsyncBabble";

	private Context context;
	private MyAsyncBabbleResponseProcess listener;
	private String path;
	private String physicalFileName;
	private String subItemFolder;
	private int statusCode;
	private boolean flagStop = false;
	private String className = "";

	// @edit : not use
	// private String messageError = "";

	/**
	 * Constructor
	 * 
	 * @param listener
	 */
	public MyAsyncBabble(Context context,
			MyAsyncBabbleResponseProcess listener, String fileName,
			String subItemFolder) {
		this.className = context.getClass().toString();
		this.context = context;
		this.listener = listener;
		this.physicalFileName = fileName;
		this.subItemFolder = subItemFolder;
		this.flagStop = false;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		listener.before();
	}

	@Override
	protected String doInBackground(String... params) {
		if (!NetworkUtility.getInstance(context).isNetworkAvailable()
				&& context != null) {
			// Return status if network is not available
			statusCode = NETWORK_STATUS_OFF;
			return null;
		}
		try {
			String url = params[0];
			InputStream is = null;
			if (url.toLowerCase(Locale.US).startsWith("https")) {
				is = MyAsyncHttpBase.openSConnection(url).getInputStream();
			} else {
				is = MyAsyncHttpBase.openConnection(url).getInputStream();
			}

			path = saveBabbleMp3File(is, physicalFileName);
			if (path != null) {
				statusCode = NETWORK_STATUS_OK;
			} else {
				statusCode = NETWORK_STATUS_ERROR;
			}
		} catch (IOException e) {
			statusCode = NETWORK_STATUS_ERROR;
			e.printStackTrace();
			// messageError = e.getMessage();
			return null;
		} catch (IOUtilException e) {
			statusCode = NETWORK_STATUS_ERROR;
			e.printStackTrace();
			// messageError = e.getMessage();
			return null;
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		// @edit : disable show error dialog
		// if (messageError.length() > 0)
		// {
		// DialogManager.alert(context, messageError);
		// // return;
		// }

		if (!flagStop)
			listener.after(statusCode, path);
		return;
	}

	/**
	 * Save babble file to sd card
	 * 
	 * @param is
	 * @param physicalBabbleName
	 * @return
	 * @throws IOException
	 * @throws IOUtilException
	 */
	private String saveBabbleMp3File(InputStream is, String physicalBabbleName)
			throws IOException, IOUtilException {
		if (!MemoryUtility.hasExternalStorage()) {
			return null;
		}
		File babbleFile = null;
		babbleFile = new File(
				ApplicationClass.ROOT_MEMORY_PATH + "/" + subItemFolder);
		if(!babbleFile.isDirectory()){
			babbleFile.mkdirs();
		}
		babbleFile = new File(
				ApplicationClass.ROOT_MEMORY_PATH + "/" + subItemFolder,
				physicalBabbleName + ".mp3");
		
		FileUtil.delete(babbleFile);
		babbleFile.createNewFile();
		// Create data output stream to write recorded data
		BufferedOutputStream ouputStream = null;
		ouputStream = new BufferedOutputStream(new FileOutputStream(babbleFile));
		DataOutputStream output = new DataOutputStream(ouputStream);
		byte[] buffer = new byte[1024];
		int read = 0;
		while ((read = is.read(buffer)) > 0) {
			output.write(buffer, 0, read);
		}
		output.close();
		is.close();
		return babbleFile.getAbsolutePath();
	}

	/**
	 * @return the flagStop
	 */
	public boolean isFlagStop() {
		return flagStop;
	}

	/**
	 * @param flagStop
	 *            the flagStop to set
	 */
	public void setFlagStop(boolean flagStop) {
		this.flagStop = flagStop;
	}
}
