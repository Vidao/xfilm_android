/*
 * Name: $RCSfile: MyAsyncHttpResponseProcess.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Dec 8, 2011 9:54:28 AM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.netviet.xfilm.utility.net;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.netviet.xfilm.R;
import com.netviet.xfilm.func.BaseActivity;
import com.netviet.xfilm.func.BaseSlideActivity;
import com.netviet.xfilm.utility.DialogManager;

import android.content.Context;

/**
 * MyAsyncHttpResponseProcess processes server response
 * 
 * @author vidp
 * 
 */
public class MyAsyncHttpResponseProcess implements MyAsyncHttpResponseListener
{
    public static final String TAG = "AsyncHttpResponseProcess";

    private BaseActivity baseActivity;

    private BaseSlideActivity baseSlideActivity;

    private Context context;

    private boolean isShowDialog = true;
    public boolean isPreventShowDialog = false;

    /**
     * @return the isShowDialog
     */
    public boolean isShowDialog()
    {
        return isShowDialog;
    }

    /**
     * @param isShowDialog the isShowDialog to set
     */
    public void setShowDialog(boolean isShowDialog)
    {
        this.isShowDialog = isShowDialog;
    }

    /**
     * Constructor
     * 
     * @param baseActivity
     */
    public MyAsyncHttpResponseProcess(BaseActivity baseActivity)
    {
        this.baseActivity = baseActivity;
        this.context = baseActivity;
    }

    public MyAsyncHttpResponseProcess(Context context)
    {
        this.context = context;
    }

    public MyAsyncHttpResponseProcess(BaseActivity baseActivity,
        boolean isShowDialog)
    {
        this.baseActivity = baseActivity;
        this.context = baseActivity;
        this.isShowDialog = isShowDialog;
    }

    public MyAsyncHttpResponseProcess(BaseSlideActivity baseSlideActivity)
    {
        this.baseSlideActivity = baseSlideActivity;
        this.context = baseSlideActivity;
    }

    public MyAsyncHttpResponseProcess(BaseSlideActivity baseSlideActivity,
        boolean isShowDialog)
    {
        this.baseSlideActivity = baseSlideActivity;
        this.context = baseSlideActivity;
        this.isShowDialog = isShowDialog;
    }

    @Override
    public void before()
    {
        // Show waiting dialog during connection
        if (isShowDialog)
        {
            if (baseActivity != null)
            {
                // baseActivity.showProgressDialog(false);
                if (!isPreventShowDialog)
                {
                    baseActivity.mHandleDialog.sendEmptyMessage(0);
                }
            }
            else if (baseSlideActivity != null)
            {
                baseSlideActivity.showProgressDialog(false);
            }
        }
    }

    @Override
    public void after(int statusCode, HttpResponse response)
    {
        // Process server response
        if (isShowDialog)
        {
            if (baseActivity != null)
            {
                baseActivity.closeProgressDialog();
            }
            else if (baseSlideActivity != null)
            {
                baseSlideActivity.closeProgressDialog();
            }
        }
        if (response == null){
            DialogManager.alert(context, R.string.server_time_out);
            return;
        }
        switch (statusCode)
        {
            case MyAsyncHttpBase.NETWORK_STATUS_OFF:
                DialogManager.alert(context,
                    R.string.message_network_is_unavailable);
                break;
            case MyAsyncHttpBase.NETWORK_STATUS_OK:
                processHttpResponse(response);
                break;
            default:
                DialogManager.alert(context, R.string.message_server_error);
        }
    }

    /**
     * Process HttpResponse
     * 
     * @param response
     */
    public void processHttpResponse(HttpResponse response)
    {
        String json = "";
        try
        {
            // Get json response
            json = EntityUtils.toString(response.getEntity(),HTTP.UTF_8);
            processIfResponseSuccess(json);
        }
        catch (Exception e)
        {
        	e.printStackTrace();
            processIfResponseFail(context.getString(R.string.message_server_error));
        }
    }

    /**
     * Interface function
     */
    public void processIfResponseSuccess(String response)
    {
        // Process if response is success
    }

    /**
     * Interface function
     */
    public void processIfResponseFail(String message)
    {
        // Process if response is fail
    }

    /**
     * Interface function
     */
    public void processIfServerError()
    {
        // Process if server error
    }
}
