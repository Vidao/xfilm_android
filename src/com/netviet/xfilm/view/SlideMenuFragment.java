package com.netviet.xfilm.view;

import com.netviet.xfilm.R;
import com.netviet.xfilm.func.BaseSlideActivity;
import com.netviet.xfilm.iview.ISlideLeftMenuListenner;
import com.netviet.xfilm.utility.NetworkUtility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnHoverListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SlideMenuFragment extends Fragment implements OnClickListener {
	ISlideLeftMenuListenner callback;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	private LinearLayout.LayoutParams params;

	private DisplayImageOptions options;
	private ImageSize imageSize;
	private ImageLoader imageLoader = ImageLoader.getInstance();

	public void setContext(BaseSlideActivity self) {
		if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
			// load image
			// Utility.setImageConfig(self, 25);
			// if (imageLoader.isInited()) {
			// imageLoader.destroy();
			// }
			// imageLoader.init(Utility.imageLoaderConfig);
			// options = getImageOption();
			// params = new LinearLayout.LayoutParams(25, 25);
			// imageSize = new ImageSize(25, 25);
			// PreferencesManager _pref = PreferencesManager.getInstance(self);
			// String avatarHref = _pref
			// .getStringValue(PreferencesManager.PREF_USER_AVARTAR_HREF);
			// RoundedImageView imgAvatar = (RoundedImageView) mainLayout
			// .findViewById(R.id.iv_avatar);
			//
			// if (avatarHref != null && avatarHref.length() > 5) {
			// avatarHref = ApplicationClass.ROOT_DATA_URL + avatarHref;
			// imageLoader.displayImage(avatarHref, imgAvatar, options);
			// }
		}

	}

	// private DisplayImageOptions getImageOption() {
	// DisplayImageOptions options = new DisplayImageOptions.Builder()
	// .showStubImage(R.drawable.ic_stub)
	// .showImageOnFail(R.drawable.loading_false)
	// .showImageForEmptyUri(R.drawable.loading_false)
	// .cacheInMemory(true).cacheOnDisc(true).build();
	// return options;
	// }

	private LinearLayout mainLayout;

	private LinearLayout profileLayout;

	private void setProfileLayoutSelected(boolean isSelected) {
		if (!((isSelected && selectedIndex == 0) || (!isSelected && selectedIndex != 0))) {
			return;
		}

		ImageView _ivProfile = (ImageView) profileLayout
				.findViewById(R.id.iv_profile);
		TextView _tvProfile = (TextView) profileLayout
				.findViewById(R.id.tv_profile);
		if (isSelected) {
			_ivProfile.setImageResource(R.drawable.setting_hover);
			_tvProfile.setTextColor(getResources().getColor(
					R.color.focused_text_color));
		} else {
			_ivProfile.setImageResource(R.drawable.setting);
			_tvProfile.setTextColor(getResources().getColor(R.color.white));
		}
	}

	private LinearLayout homeLayout;

	private void setHomeLayoutSelected(boolean isSelected) {
		if (!((isSelected && selectedIndex == 1) || (!isSelected && selectedIndex != 1))) {
			return;
		}

		ImageView _ivHome = (ImageView) homeLayout.findViewById(R.id.iv_home);
		TextView _tvHome = (TextView) homeLayout.findViewById(R.id.tv_home);
		if (isSelected) {
			_ivHome.setImageResource(R.drawable.home_hover);
			homeLayout.setBackgroundColor(getResources().getColor(
					R.color.actived_layout));
			_tvHome.setTextColor(getResources().getColor(
					R.color.focused_text_color));
		} else {
			homeLayout.setBackgroundColor(getResources().getColor(
					R.color.trans));
			_ivHome.setImageResource(R.drawable.home);
			_tvHome.setTextColor(getResources().getColor(R.color.white));
		}
	}

	private LinearLayout titleLayout;

	private void setTitleLayoutSelected(boolean isSelected) {
		if (!((isSelected && selectedIndex == 2) || (!isSelected && selectedIndex != 2))) {
			return;
		}

		ImageView _ivTittle = (ImageView) titleLayout
				.findViewById(R.id.iv_title);
		TextView _tvTitle = (TextView) titleLayout.findViewById(R.id.tv_title);
		if (isSelected) {
			titleLayout.setBackgroundColor(getResources().getColor(
					R.color.actived_layout));
			_ivTittle.setImageResource(R.drawable.film_hover);
			_tvTitle.setTextColor(getResources().getColor(
					R.color.focused_text_color));
		} else {
			titleLayout.setBackgroundColor(getResources().getColor(
					R.color.trans));
			_ivTittle.setImageResource(R.drawable.film);
			_tvTitle.setTextColor(getResources().getColor(R.color.white));
		}

	}

	private LinearLayout historyLayout;

	private void setHistoryLayoutSelected(boolean isSelected) {
		if (!((isSelected && selectedIndex == 3) || (!isSelected && selectedIndex != 3))) {
			return;
		}

		ImageView _ivHistory = (ImageView) historyLayout
				.findViewById(R.id.iv_history);
		TextView _tvHistory = (TextView) historyLayout
				.findViewById(R.id.tv_history);
		if (isSelected) {
			historyLayout.setBackgroundColor(getResources().getColor(
					R.color.actived_layout));
			_ivHistory.setImageResource(R.drawable.history_hover);
			_tvHistory.setTextColor(getResources().getColor(
					R.color.focused_text_color));
		} else {
			historyLayout.setBackgroundColor(getResources().getColor(
					R.color.trans));
			_ivHistory.setImageResource(R.drawable.history);
			_tvHistory.setTextColor(getResources().getColor(R.color.white));
		}

	}

	private LinearLayout hintLayout;

	private void setHintLayoutSelected(boolean isSelected) {
		if (!((isSelected && selectedIndex == 4) || (!isSelected && selectedIndex != 4))) {
			return;
		}

		ImageView _ivHint = (ImageView) hintLayout.findViewById(R.id.iv_hint);
		TextView _tvHint = (TextView) hintLayout.findViewById(R.id.tv_hint);
		if (isSelected) {
			hintLayout.setBackgroundColor(getResources().getColor(
					R.color.actived_layout));
			_ivHint.setImageResource(R.drawable.hint_hover);
			_tvHint.setTextColor(getResources().getColor(
					R.color.focused_text_color));
		} else {
			hintLayout.setBackgroundColor(getResources().getColor(
					R.color.trans));
			_ivHint.setImageResource(R.drawable.hint);
			_tvHint.setTextColor(getResources().getColor(R.color.white));
		}

	}

	private LinearLayout favouriteLayout;

	private void setFavouriteSelected(boolean isSelected) {
		if (!((isSelected && selectedIndex == 5) || (!isSelected && selectedIndex != 5))) {
			return;
		}

		ImageView _ivFavourite = (ImageView) favouriteLayout
				.findViewById(R.id.iv_favourite);
		TextView _tvFavourite = (TextView) favouriteLayout
				.findViewById(R.id.tv_favourite);
		if (isSelected) {
			favouriteLayout.setBackgroundColor(getResources().getColor(
					R.color.actived_layout));
			_ivFavourite.setImageResource(R.drawable.favourite_hover);
			_tvFavourite.setTextColor(getResources().getColor(
					R.color.focused_text_color));
		} else {
			favouriteLayout.setBackgroundColor(getResources().getColor(
					R.color.trans));
			_ivFavourite.setImageResource(R.drawable.favourite);
			_tvFavourite.setTextColor(getResources().getColor(R.color.white));
		}

	}

	private LinearLayout notifyLayout;

	private void setNotifySelected(boolean isSelected) {
		if (!((isSelected && selectedIndex == 6) || (!isSelected && selectedIndex != 6))) {
			return;
		}

		ImageView _ivNotify = (ImageView) notifyLayout
				.findViewById(R.id.iv_notify);
		TextView _tvNotify = (TextView) notifyLayout
				.findViewById(R.id.tv_notify);
		if (isSelected) {
			notifyLayout.setBackgroundColor(getResources().getColor(
					R.color.actived_layout));
			_ivNotify.setImageResource(R.drawable.notifi_hover);
			_tvNotify.setTextColor(getResources().getColor(
					R.color.focused_text_color));
		} else {
			notifyLayout.setBackgroundColor(getResources().getColor(
					R.color.trans));
			_ivNotify.setImageResource(R.drawable.notifi);
			_tvNotify.setTextColor(getResources().getColor(R.color.white));
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainLayout = (LinearLayout) inflater.inflate(R.layout.slide_menu,
				container, false);

		profileLayout = (LinearLayout) mainLayout
				.findViewById(R.id.layout_profile);
		profileLayout.setOnClickListener(this);

		homeLayout = (LinearLayout) mainLayout.findViewById(R.id.layout_home);
		homeLayout.setOnClickListener(this);

		titleLayout = (LinearLayout) mainLayout.findViewById(R.id.layout_title);
		titleLayout.setOnClickListener(this);
		historyLayout = (LinearLayout) mainLayout
				.findViewById(R.id.layout_history);
		historyLayout.setOnClickListener(this);
		hintLayout = (LinearLayout) mainLayout.findViewById(R.id.layout_hint);
		hintLayout.setOnClickListener(this);
		favouriteLayout = (LinearLayout) mainLayout
				.findViewById(R.id.layout_favourite);
		favouriteLayout.setOnClickListener(this);
		notifyLayout = (LinearLayout) mainLayout
				.findViewById(R.id.layout_notify);
		notifyLayout.setOnClickListener(this);
		return mainLayout;
	}

	private int selectedIndex = 1;

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int _viewID = v.getId();
		switch (_viewID) {
		case R.id.layout_home:
			callback.homeSelectedListener();
			selectedIndex = 1;
			break;
		case R.id.layout_profile:
			callback.profileSelectedListener();
			selectedIndex = 0;
			break;
		case R.id.layout_title:
			callback.profileSelectedListener();
			selectedIndex = 2;
			break;
		case R.id.layout_history:
			callback.profileSelectedListener();
			selectedIndex = 3;
			break;
		case R.id.layout_hint:
			callback.profileSelectedListener();
			selectedIndex = 4;
			break;
		case R.id.layout_favourite:
			callback.profileSelectedListener();
			selectedIndex = 5;
			break;
		case R.id.layout_notify:
			callback.profileSelectedListener();
			selectedIndex = 6;
			break;
		default:
			break;
		}

		processSelectedColor();
	}

	private void processSelectedColor() {
		switch (selectedIndex) {
		case 0:
			setProfileLayoutSelected(true);
			setHomeLayoutSelected(false);
			setTitleLayoutSelected(false);
			setHistoryLayoutSelected(false);
			setHintLayoutSelected(false);
			setFavouriteSelected(false);
			setNotifySelected(false);
			break;
		case 1:

			setProfileLayoutSelected(false);
			setHomeLayoutSelected(true);
			setTitleLayoutSelected(false);
			setHistoryLayoutSelected(false);
			setHintLayoutSelected(false);
			setFavouriteSelected(false);
			setNotifySelected(false);
			break;
		case 2:
			setProfileLayoutSelected(false);
			setHomeLayoutSelected(false);
			setTitleLayoutSelected(true);
			setHistoryLayoutSelected(false);
			setHintLayoutSelected(false);
			setFavouriteSelected(false);
			setNotifySelected(false);

			break;
		case 3:
			setProfileLayoutSelected(false);
			setHomeLayoutSelected(false);
			setTitleLayoutSelected(false);
			setHistoryLayoutSelected(true);
			setHintLayoutSelected(false);
			setFavouriteSelected(false);
			setNotifySelected(false);

			break;
		case 4:
			setProfileLayoutSelected(false);
			setHomeLayoutSelected(false);
			setTitleLayoutSelected(false);
			setHistoryLayoutSelected(false);
			setHintLayoutSelected(true);
			setFavouriteSelected(false);
			setNotifySelected(false);

			break;
		case 5:
			setProfileLayoutSelected(false);
			setHomeLayoutSelected(false);
			setTitleLayoutSelected(false);
			setHistoryLayoutSelected(false);
			setHintLayoutSelected(false);
			setFavouriteSelected(true);
			setNotifySelected(false);

			break;
		case 6:
			setProfileLayoutSelected(false);
			setHomeLayoutSelected(false);
			setTitleLayoutSelected(false);
			setHistoryLayoutSelected(false);
			setHintLayoutSelected(false);
			setFavouriteSelected(false);
			setNotifySelected(true);

			break;

		default:
			break;
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (ISlideLeftMenuListenner) activity;
		} catch (Exception e) {
		}
	}

}
