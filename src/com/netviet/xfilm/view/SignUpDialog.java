package com.netviet.xfilm.view;

import com.netviet.xfilm.R;
import com.netviet.xfilm.func.BaseActivity;
import com.netviet.xfilm.func.BaseSlideActivity;
import com.netviet.xfilm.utility.ToastManager;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class SignUpDialog extends Dialog implements
		android.view.View.OnClickListener {

	private BaseActivity self;
	private BaseSlideActivity slideSelf;
	private Handler parentHandler;

	public Handler getParentHandler() {
		return parentHandler;
	}

	public static final int HANDLER_SIGNUP_PROCESS = 2;
	public static final int HANDLER_SIGNUP_RESPONSE = 3;

	public void setParentHandler(Handler parentHandler) {
		this.parentHandler = parentHandler;
	}

	public SignUpDialog(Context context, BaseActivity self) {
		super(context);
		this.self = self;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sign_up_page);
		initUI();
	}

	public SignUpDialog(Context context, BaseSlideActivity self) {
		super(context);
		this.slideSelf = self;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		setContentView(R.layout.sign_up_page);
		initUI();
	}

	private EditText et_signUpUsername;
	private EditText et_signUpPassWord;
	private EditText et_signUpPassWordConfirm;
	private EditText et_signUpPhone;

	private void initUI() {
		LinearLayout layoutMain = (LinearLayout) findViewById(R.id.layoutMainSignup);

		layoutMain.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				InputMethodManager inputManager = null;
				View view = null;

				if (self != null) {
					inputManager = (InputMethodManager) self
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					view = self.getCurrentFocus();
				} else {
					inputManager = (InputMethodManager) slideSelf
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					view = slideSelf.getCurrentFocus();
				}
				if (view != null) {
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				}
				return false;
			}
		});
		Button _bt_back = (Button) findViewById(R.id.bt_regist_back);
		_bt_back.setOnClickListener(this);
		Button _bt_regist = (Button) findViewById(R.id.bt_regist);
		_bt_regist.setOnClickListener(this);
		et_signUpUsername = (EditText) findViewById(R.id.et_signup_username);
		et_signUpPassWord = (EditText) findViewById(R.id.et_signup_password);
		et_signUpPassWordConfirm = (EditText) findViewById(R.id.et_signup_password_confirm);
		et_signUpPhone = (EditText) findViewById(R.id.et_signup_phone);
		setCancelable(false);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int _viewID = v.getId();
		switch (_viewID) {
		case R.id.bt_regist_back:
			dismiss();
			break;
		case R.id.bt_regist:
			String _userName = et_signUpUsername.getText().toString();
			String _passWord = et_signUpPassWord.getText().toString();
			String _passWordConfirm = et_signUpPassWordConfirm.getText()
					.toString();
			String _phoneNumber = et_signUpPhone.getText().toString();
			Bundle sign_up_bundle = new Bundle();
			sign_up_bundle.putString("USERNAME", _userName);
			sign_up_bundle.putString("PASSWORD", _passWord);
			sign_up_bundle.putString("PHONENUMBER", _phoneNumber);
			Message msg = Message.obtain();
			msg.what = HANDLER_SIGNUP_PROCESS;
			msg.setData(sign_up_bundle);
			if (checkSignUpInfo(_userName, _passWord, _passWordConfirm)) {
				parentHandler.sendMessage(msg);
			}
			break;
		default:
			break;
		}

	}

	private boolean checkSignUpInfo(String userName, String passWord,
			String passConfirm) {
		boolean returnValue = false;
		Context context = null;
		if (slideSelf == null) {
			context = self.getApplicationContext();
		} else if (self == null) {
			context = slideSelf.getApplicationContext();
		}

		if (userName.length() < 6) {
			returnValue = false;
			ToastManager.showLongToastMessage(context,
					context.getString(R.string.signup_user_length_invalid));
		} else if (passWord.length() < 6 && passConfirm.length() < 6) {
			returnValue = false;
			ToastManager.showLongToastMessage(context,
					context.getString(R.string.signup_pass_length_invalid));
		} else if (!passWord.equals(passConfirm)) {
			returnValue = false;
			ToastManager.showLongToastMessage(context,
					context.getString(R.string.signup_pass_not_match));
		} else {
			returnValue = true;
		}
		return returnValue;
	}

}
