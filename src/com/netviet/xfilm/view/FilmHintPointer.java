package com.netviet.xfilm.view;

import com.netviet.xfilm.ApplicationClass;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

public class FilmHintPointer extends View {

	private Paint paint;
	private int numberOfHint = 5;

	public int getNumberOfHint() {
		return numberOfHint;
	}

	public void setNumberOfHint(int numberOfHint) {
		this.numberOfHint = numberOfHint;
	}

	public FilmHintPointer(Context context, AttributeSet attrs) {
		super(context, attrs);
		paint = new Paint();
		paint.setStyle(Style.FILL_AND_STROKE);
		paint.setColor(Color.WHITE);
		activedIndex = (numberOfHint / 2 + numberOfHint % 2) - 1;
		centedIndex = activedIndex;
		// TODO Auto-generated constructor stub
	}

	public int getActivedIndex() {
		return activedIndex;
	}

	public void setActivedIndex(int activedIndex) {
		this.activedIndex = activedIndex;
		invalidate();
	}

	private final int radius = 10;
	private int xCenter = ApplicationClass.SCREEN_WIDTH / 2;
	private final int yCenter = 20;
	private int activedIndex;
	private int centedIndex;

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		for (int i = centedIndex; i < numberOfHint; i++) {
			if (i == activedIndex) {
				paint.setColor(Color.WHITE);
			} else {
				paint.setColor(Color.GRAY);
			}
			canvas.drawCircle(xCenter + (i - centedIndex) * 3 * radius,
					yCenter, radius + (i-centedIndex), paint);
		}
		for (int i = centedIndex - 1; i >= 0; i--) {
			if (i == activedIndex) {
				paint.setColor(Color.WHITE);
			} else {
				paint.setColor(Color.GRAY);
			}
			canvas.drawCircle(xCenter + ((i - centedIndex) * 3 * radius),
					yCenter, radius - (i-centedIndex), paint);
		}
		super.onDraw(canvas);
	}

	public void setXcenter() {
		xCenter = ApplicationClass.SCREEN_WIDTH / 2;
		invalidate();
	}

}
