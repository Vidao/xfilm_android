package com.netviet.xfilm.view;

import com.google.android.gms.internal.cb;
import com.netviet.xfilm.R;
import com.netviet.xfilm.func.BaseActivity;
import com.netviet.xfilm.func.BaseSlideActivity;
import com.netviet.xfilm.utility.PreferencesManager;
import com.netviet.xfilm.utility.ToastManager;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LoginDialog extends Dialog implements
		android.view.View.OnClickListener {

	public Handler parentHandler;

	public Handler getParentHandler() {
		return parentHandler;
	}
	public static final int HANDLER_SIGNUP = 1;
	public static final int HANDLER_LOGIN = 0;

	public void setParentHandler(Handler parentHandler) {
		this.parentHandler = parentHandler;
	}

	private BaseActivity self;
	private BaseSlideActivity slideSelf;
	private Context context;

	public LoginDialog(Context context, BaseActivity self) {
		super(context);
		this.self = self;
		this.context = context;
		this.slideSelf = null;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sign_in_page);
		initUI();
	}

	public LoginDialog(Context context, BaseSlideActivity self) {
		super(context);
		this.slideSelf = self;
		this.context = context;
		this.self  = null;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		setContentView(R.layout.sign_in_page);
		initUI();
	}

	private EditText userNameTxt;
	private EditText passwordTxt;
	private CheckBox cb_saveUserPass;

	private void initUI() {
		
		
		Button btnBack = (Button) findViewById(R.id.backBtn);
		btnBack.setOnClickListener(this);

		Button btnLogin = (Button) findViewById(R.id.loginBtn);
		btnLogin.setOnClickListener(this);
		cb_saveUserPass = (CheckBox) findViewById(R.id.cb_save_user_pass);
		LinearLayout layoutMain = (LinearLayout) findViewById(R.id.layoutMainSignin);
		layoutMain.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				View view = null;
				InputMethodManager inputManager = null;
				if (self != null) {
					inputManager = (InputMethodManager) self
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					view = self.getCurrentFocus();
				} else {
					inputManager = (InputMethodManager) slideSelf
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					view = slideSelf.getCurrentFocus();
				}
				if (view != null) {
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				}
				return false;
			}
		});
		userNameTxt = (EditText) findViewById(R.id.userNameTxt);
		passwordTxt = (EditText) findViewById(R.id.passwordTxt);
		TextView tv_signUp = (TextView) findViewById(R.id.tv_regist);
		tv_signUp.setOnClickListener(this);
		PreferencesManager _pref  = null;
		if(self != null){
			 _pref = PreferencesManager.getInstance(self);
		}else{
			 _pref = PreferencesManager.getInstance(slideSelf);
		}
		
		if (_pref.getBooleanValue(PreferencesManager.PREF_IS_SAVE_ACCOUNT)) {
			cb_saveUserPass.setChecked(true);
			userNameTxt.setText(_pref
					.getStringValue(PreferencesManager.PREF_USER_NAME));
		}
		setCancelable(false);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		dismiss();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int _viewID = v.getId();
		if (parentHandler == null) {
			return;
		}
		
		switch (_viewID) {
		case R.id.tv_regist:
			// send parent handler sign up here;
			parentHandler.sendEmptyMessage(HANDLER_SIGNUP);
			break;
		case R.id.loginBtn:
			// send parent handler login here
			Bundle loginBundle = new Bundle();
			String _userName = userNameTxt.getText().toString();
			String _passWord = passwordTxt.getText().toString();
			loginBundle.putString("USERNAME", _userName);
			loginBundle.putString("PASSWORD", _passWord);
			if(_userName== null || _userName.length()<6 ){
				System.out.println("conntext: :: : : : "+context);
				ToastManager.showLongToastMessage(context, context.getString(R.string.signup_user_length_invalid));
				return;
			}else if(_passWord ==null || _passWord.length()<6 ){
				ToastManager.showLongToastMessage(context, context.getString(R.string.signup_pass_length_invalid));
				return;
			}
			loginBundle.putBoolean("ISSAVING", cb_saveUserPass.isChecked());
			Message msg = Message.obtain();
			msg.setData(loginBundle);
			msg.what = HANDLER_LOGIN;
			parentHandler.sendMessage(msg);
			break;
		case R.id.backBtn:
			dismiss();
			break;
		default:
			break;
		}

	}

}
